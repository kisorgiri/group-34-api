const router = require('express').Router();
// import routing level middleware
const AuthRouter = require('./controllers/auth.controller');
const UserRouter = require('./controllers/user.controller')
const ProductRouter = require('./modules/products/product.route');

// import application level middleware
const authenticate = require('./middlewares/authenticate');
const authorize = require('./middlewares/authorize');

router.use('/auth', AuthRouter);
router.use('/user', authenticate, UserRouter)
router.use('/product', ProductRouter)

module.exports = router;

