const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const dbName = 'group34db';
const conxnURL = 'mongodb://localhost:27017';
const OID = mongodb.ObjectID;

module.exports = {
    MongoClient,
    dbName,
    conxnURL,
    OID
}
