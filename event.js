const events = require('events');

const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();

// listener
myEvent.on('broadway', function (data) {
    console.log('at broadway event', data);
})
// trigger

setTimeout(function () {
    myEvent1.emit('broadway','hi at events')
}, 1000)

