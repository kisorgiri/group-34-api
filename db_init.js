// db connection setup
const dbConfig = require('./configs/db.config');
const mongoose = require('mongoose');

// mongodb://localhost:27017/db_name
mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})
mongoose.connection.once('open', function (data) {
    console.log('db connection open');
})
mongoose.connection.on('error', function (err) {
    console.log('error connecting to db ', err)
})
