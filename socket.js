// socket related stuffs
const socket = require('socket.io');
const config = require('./configs');
var users = [];
module.exports = function (app) {

    const io = socket(app.listen(config.SOCKET_PORT), {
        cors: {
            accept: '/*'
        }
    });
    io.on('connection', function (client) {
        var id = client.id;
        console.log('socket client connected to server')
        client.on('new-user', function (username) {
            users.push({
                id: id,
                name: username
            })
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        // .emit () for self connected client
        // .broadcast.emit() // other then self one
        // .broadcast.to(socket id).emit() // for specific client (private message)
        client.on('new-message', function (data) {
            console.log('new messag e>>', data)
            client.emit('reply-message-own', data);
            client.broadcast.to(data.receiverId).emit('reply-message', data);
        })

        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (user.id === id) {
                    users.splice(index, 1);
                }
            })
            client.broadcast.emit('users', users)
        })
    })
}
