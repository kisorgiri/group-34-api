const fs = require('fs'); //es5
// node_modules's packages and nodejs inbuilt packages lai import garda path dinu pardaina

// write

// fs.writeFile('./files/a.txt', 'welcome to nodejs', function (err, done) {
//     if (err) {
//         console.log('error writing file', err);
//     } else {
//         console.log('success in writing file >>', done)
//     }
// })



// const fileOp = require('./write');
// console.log('fileop is >>', fileOp)


// // code execution
// fileOp.a('kishor.txt', 'i am good today', function (err, done) {
//     if (err) {
//         console.log('error in my write >>', err);
//     } else {
//         console.log('succes sin my write ', done);
//     }
// })

// read
// fs.readFile('./files/a.txt', 'UTF-8', function (err, done) {
//     if (err) {
//         console.log('error in read is >>', err);
//     } else {
//         console.log('success in read file>', done);
//     }
// })

// rename
// fs.rename('./files/a.txt', './files/abcd.txt', function (err, done) {
//     if (err) {
//         console.log('error in renamee >>', err);
//     } else {
//         console.log('success in rename', done);
//     }
// })


// remove
fs.unlink('./files/b.txt', function (err, done) {
    if (err) {
        console.log('error in removing >>', err);
    } else {
        console.log('success in removing >>', done)
    }
})

// task

// make it functional
// handle result of your function (promise or callback)
// seperate task and execution in different file


