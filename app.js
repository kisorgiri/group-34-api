const express = require('express');
const morgan = require('morgan');
const path = require('path');
const config = require('./configs')
const cors = require('cors');
require('./db_init');

// import express from 'express' // es6
const app = express();
// entire express framework will be available in app

// events stuff
const events = require('events');
const myEvent = new events.EventEmitter();

myEvent.on('error', function (err, res) {
    console.log('here at  error event', err)
    res.json(err);
})

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    // add my event in each and every request
    next();
})

// part of app.js run socket.js file
require('./socket')(app)

// load API route
const API_ROUTER = require('./api.route')

// load thirdparty middleware
app.use(cors()); // enable all requests
app.use(morgan('dev'))

// load inbuilt middleware
// serve static file
app.use(express.static('uploads')) //internal serve
// urlencoded data
app.use(express.urlencoded({
    extended: true
}))

// json parser
app.use(express.json())
// add incoming data  into req.body property

app.use('/files', express.static(path.join(process.cwd(), 'uploads')));

app.use('/api', API_ROUTER)

// 404 error handler
app.use(function (req, res, next) {
    next({
        msg: 'Not Found',
        status: 404
    })
})
// error handling middleware
// error handling middleware doesnot came into action inbetween req-res cycle
// so we have to call it to execute the middleware
// calling next with argument will execute error handling middleware
// middleware with 4 arguments
// 1st argument place holder is for error
// req,res,next ==> are similar to application level middleware
app.use(function (err, req, res, next) {
    console.log('error is >>', err)
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

// server listener
app.listen(config.PORT, function (err, done) {
    if (err) {
        console.log('server listening failed', err)
    } else {
        console.log('server listening at port ' + config.PORT)
        console.log('press CTRL + C to exit')
    }
})



// middleware
// middleware is a function that has access to 
// http request object
// http response object
// and next middleware function reference

// middleware can modify http request and response object

// middleware's always came into action in between req -res cycle

// middleware's order is very very important

// to configure middlewre we use several methods of express

// use, http verb, all

// syntax
// function(req,res,next){
//     // req or 1st arg placeholder is for http request
//     // res or 2nd arg placeholder is for http response
//     // next or 3rd arg placeholder is for next middleware function reference
// }

// app.use(middleware function)
// use method is used to configure middleware

// types of middleware
// 1. application level middleware
// 2.routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5.error handling middleware


// 1. application level middleware
// middleware having direct scope of req res and next is application level middleware
// above example of checkTIcket and validateTicket are application level middleware

