const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.query['token'])
        token = req.query['token']
    if (!token) {
        return next({
            msg: 'Authentication Failed,Token not provided',
            status: 401
        })
    }
    // token verification
    jwt.verify(token, config.JWT_SECRECT, function (err, decoded) {
        if (err) {
            return next(err);
        }
        console.log('token verification success full ...', decoded)
        // middleware ho so request must be proceed further
        UserModel.findById(decoded._id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User removed from system',
                    status: 400
                })
            }
            req.user = user;
            next();
        })
    })
}


// prepare another middleware to authorize
// role 1 cha vane next garne else req-res cycle complete
