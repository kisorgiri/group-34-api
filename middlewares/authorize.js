module.exports = function (req, res, next) {
    if (req.user.role != 1) {
        return next({
            msg: "You dont have permission",
            status: 403
        })
    }
    next();
}
